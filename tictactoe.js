var readline = require('readline');
var reader = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

function Human(){}

Human.prototype.getMove = function(callback) {
  reader.question("Input your move: ", function(input) {

    var pos_x = parseInt(input[0]);
    var pos_y = parseInt(input[1]);
    callback([pos_x, pos_y]);
  });
}

function Game(player1_input, player2_input) {
  this.player1 = player1_input;
  this.player2 = player2_input;

  this.grid = new Array(3);
  for (var i = 0; i < 3; i++) {
    this.grid[i] = ["", "", ""];
  }

}

Game.prototype.run = function(endGameCallBack) {
  thisGame = this;
  gameLoop(thisGame.player1, false);

  function gameLoop(current_player, gameover){
    thisGame.render();
    if (!gameover){
      thisGame.makeMove(current_player, function(next_player, gameover) { gameLoop(next_player, gameover) } );
    } else {
      endGameCallBack(thisGame.grid);
    }
  }
}

Game.prototype.render = function() {
  for (var i = 0; i < 3; i++) {
    console.log(this.grid[i]);
  }
}

Game.prototype.makeMove = function(player, callbackToGameLoop)  {
  thisGame = this;
  if(player == thisGame.player1) {
      var mark = "X";
    } else {
      var mark = "O";
    };

  thisGame.checkValidMove(player, function(valid, position){
    if (valid) {
      thisGame.grid[position[0]][position[1]] = mark;
    }

    if(player == thisGame.player1) {
      next_player = thisGame.player2;
    } else {
      next_player = thisGame.player1;
    }

    callbackToGameLoop(next_player, thisGame.checkGameOver());

  });

}

Game.prototype.checkValidMove = function(player, somecallback) {
  thisGrid = this;
  player.getMove(function( x ){
    var position = x;

    if (thisGrid.grid[position[0]][position[1]] == "") {
      console.log("A");
      somecallback(true, position);
    }
    else {
      console.log("B");
      somecallback(false, position);
    }
  });

}


Game.prototype.checkGameOver = function() {
  thisGame = this;
  console.log(thisGame.checkRows());
  console.log(thisGame.checkColumns());
  console.log(thisGame.checkDiags());

  return (thisGame.checkRows() || thisGame.checkColumns() || thisGame.checkDiags())
  // if (checkRows() || checkColumns() || checkDiags()) {
//     return true
//   }
//   return false
}

Game.prototype.checkRows = function() {
  thisGame = this;
  for (var i = 0; i < 3; i++) {
    if ((thisGame.grid[i][0] == thisGame.grid[i][1]) && (thisGame.grid[i][1] == thisGame.grid[i][2]) && (thisGame.grid[i][2] != "")) {

      return true;
    }
  }
  return false;
}

Game.prototype.checkColumns = function() {
  thisGame = this;
  for (var i = 0; i < 3; i++) {
    if ((thisGame.grid[0][i] == thisGame.grid[1][i]) && (thisGame.grid[1][i] == thisGame.grid[2][i]) && (thisGame.grid[2][i] != "")) {
      return true;
    }
  }
  return false;
}

Game.prototype.checkDiags = function() {
  thisGame = this;
  if ((thisGame.grid[0][0] == thisGame.grid[1][1]) && (thisGame.grid[1][1] == thisGame.grid[2][2]) && (thisGame.grid[2][2] != "")) {
    return true;
  }
  if ((thisGame.grid[2][0] == thisGame.grid[1][1]) && (thisGame.grid[1][1] == thisGame.grid[0][2]) && (thisGame.grid[0][2] != "")) {
    return true;
  }
  return false;
}

h1 = new Human;
h2 = new Human;
g = new Game(h1,h2);
g.run(function(x) {console.log(x)});

