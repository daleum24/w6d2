var readline = require('readline');
var reader = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});


(function (root) {
  var Hanoi = root.Hanoi = (root.Hanoi || {});
  //var Game  = root.Game = (root.Game || {});

  var Game = Hanoi.Game = function Game() {
    this.tower1 = [1,2,3];
    this.tower2 = [];
    this.tower3 = [];
    var thisGame = this;
  }

  Game.prototype.run = function(endGameCallBack){
    var thisGame = this;
    gameLoop(false);

    function gameLoop (gameover){
      thisGame.render();
      if (!gameover){
        // thisGame.getMove(function(x) { gameLoop(x) };
        thisGame.makeMove(function(x) { gameLoop(x) } );
      }
      else {
        endGameCallBack(thisGame.tower3);
      }
    }
  };

  Game.prototype.makeMove = function(callback) {
    var thisGame = this;
    thisGame.checkValidMove(function(valid, s, d){
      console.log(valid);
      if (valid){
        var piece = s.shift();
        d.unshift(piece);
      }
      callback(thisGame.checkGameOver());
    });
  }

  Game.prototype.checkValidMove = function(callback) {
    var thisGame = this;
    thisGame.getMove(function(s, d){
      var piece = s[0];

      if (!piece) {
        callback(false, s, d);
      }
      else if (!d[0]) {
        callback(true, s, d);
      }
      else if (piece < d[0]) {
        callback(true, s, d);
      }
      else {
        callback(false, s, d);
      }
    });
  }

  Game.prototype.checkGameOver = function() {
    var thisGame = this;
    var sorted = true;
    for (var i = 0; i < 3; i++) {
      if(i+1 != thisGame.tower3[i]) {
        sorted = false;
      }
    }
    return sorted;
  }

  Game.prototype.render = function(){
    console.log("Tower 1: " + this.tower1);
    console.log("Tower 2: " + this.tower2);
    console.log("Tower 3: " + this.tower3);
  };

  Game.prototype.getMove = function(callback) {
    var thisGame = this;
    reader.question("Enter Source", function(source_input) {

      reader.question("Enter Destination", function(dest_input) {


        var source_int = parseInt(source_input);
        var destination_int = parseInt(dest_input);

        switch(source_int) {
        case 1:

          var source_obj = thisGame.tower1;

          break;
        case 2:
          var source_obj = thisGame.tower2;
          break;
        case 3:
          var source_obj = thisGame.tower3;
          break;
        }

        switch(destination_int) {
        case 1:
          var dest_obj = thisGame.tower1;
          break;
        case 2:
          var dest_obj = thisGame.tower2;
          break;
        case 3:
          var dest_obj = thisGame.tower3;
          break;
        }

        callback(source_obj, dest_obj);

      });
    });
  }
})(this);

var g = new this.Hanoi.Game();
//g.getMove(function(s, d) {console.log("this is s" + s + " " + "this is s" + d)});
g.run(function(x) {console.log(x)})